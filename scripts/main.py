import multiprocessing as mp


def exec(*args):
  try:
    import subprocess
    return subprocess.check_output(list(args)).decode('utf-8')
  except:
    import traceback
    return traceback.format_exc()

def run(progress_queue, pid, device, memory_fraction, dataset_name, model_name, n_iterations, root, override=False):
  import tensorflow as tf
  from craynn import get_gpu_session
  from ope.experiment.master_list import ope_models, two_class_datasets, one_class_models, one_class_datasets
  from ope.experiment import ExperimentInitializer

  tf.logging.set_verbosity(tf.logging.INFO)

  session = get_gpu_session(device, memory_fraction=memory_fraction)

  try:
    if model_name in ope_models:
      dataset, partitions, repeats = two_class_datasets[dataset_name]
      model = ope_models[model_name]
    else:
      dataset, partitions, repeats = one_class_datasets[dataset_name]
      model = one_class_models[model_name]

    experiment_init = ExperimentInitializer(
      session, dataset,
      model, partitions, repeats,
      root=root,
      override=override
    )

    total = len(experiment_init)

    for i in experiment_init.initialize():
      progress_queue.put((pid, (0, 'init-%d/%d' % (i, total))))

    experiment = experiment_init()
    progress_queue.put((pid, (0, None)))

    session.graph.finalize()

    for i in experiment.train(n_iterations):

      if progress_queue is not None:
        progress_queue.put((pid, (i + 1, None)))

    if progress_queue is not None:
      progress_queue.put((pid, None))

  finally:
    session.close()

def worker(slot_queue, progress_queue, pid, dataset_name, model_name, n_iterations, root, log_dir=None, override=False):
  import os
  import sys

  if log_dir is not None:
    os.makedirs(log_dir, exist_ok=True)

    path = os.path.join(log_dir, '%s.%s.%d.out' % (dataset_name, model_name, pid))
    sys.stdout = open(path, "a")

    path = os.path.join(log_dir, '%s.%s.%d.err' % (dataset_name, model_name, pid))
    sys.stderr = open(path, "a")

  device, memory_fraction = slot_queue.get()

  try:
    run(progress_queue, pid, device, memory_fraction, dataset_name, model_name, n_iterations, root, override=override)
  except:
    import traceback
    traceback.print_exc(file=sys.stderr)

    trace = traceback.format_exc()
    progress_queue.put((pid, trace))

  finally:
    slot_queue.put((device, memory_fraction))

def warmup(datasets, queue):
  from ope.experiment import one_class_datasets, two_class_datasets

  for dataset in datasets:
    task, _, _ = (two_class_datasets if dataset in two_class_datasets else one_class_datasets)[dataset]

    try:
      data = task()
      del data
    except:
      import traceback
      import sys
      traceback.print_exc(file=sys.stderr)
      trace = traceback.format_exc()
      stats = exec('nvidia-smi')

      queue.put('%s\n%s' % (trace, stats))
    else:
      queue.put(None)

def launch_warmup(datasets):
  from tqdm import tqdm

  queue = mp.Queue()
  warmup_bar = tqdm(total=len(datasets), desc='warmup', leave=False)

  warmup_task = mp.Process(target=warmup, args=(datasets, queue))
  warmup_task.start()

  for _ in datasets:
    result = queue.get()
    if result is not None:
      print(result)

    warmup_bar.update()

  warmup_task.join()

def launch(devices, memory_fraction, tasks, progress=True):
  import numpy as np
  from tqdm import tqdm

  slot_queue = mp.Queue()

  if memory_fraction is not None:
    processes_per_device = int(np.floor(1 / memory_fraction))
  else:
    processes_per_device = 1

  for device in devices:
    for _ in range(processes_per_device):
      slot_queue.put((device, memory_fraction))

  if progress:
    progress_bars = []
  else:
    progress_bars = None
  names = []
  processes = []

  progress_queue = mp.Queue()

  max_name_len = max([ len('%s/%s' % task[:2]) for task in tasks ])

  for pid, task in enumerate(tasks):
    dataset_name, model_name, n_iterations = task[:3]
    name = ('%s/%s' % (dataset_name, model_name)).ljust(max_name_len)
    names.append(name)

    if progress:
      progress_bars.append(tqdm(
        total=n_iterations,
        desc=name
      ))

    proc = mp.Process(target=worker, args=(slot_queue, progress_queue, pid,) + task)
    processes.append(proc)
    proc.start()

  while any([proc.is_alive() for proc in processes]):
    pid, status = progress_queue.get()

    if status is None:
      if progress:
        progress_bars[pid].update(progress_bars[pid].total - progress_bars[pid].n)

        for pb in progress_bars:
          pb.refresh()

      else:
        print('%s has finished successfully.' % (names[pid], ))
        print('Alive tasks:')
        print(', '.join([
          names[i] for i, proc in enumerate(processes)
          if proc.is_alive()
        ]))

      processes[pid].join()

    elif isinstance(status, tuple):
      i, text = status

      if progress:

        progress_bars[pid].update(i - progress_bars[pid].n)

        if text is not None:
          progress_bars[pid].set_description('%s [%s]' % (names[pid], text))
        else:
          progress_bars[pid].set_description(names[pid])

        for pb in progress_bars:
          pb.refresh()
      else:
        if text is not None:
          print('%s [%d]: %s.' % (names[pid], i, text))
        else:
          print('%s [%d]' % (names[pid], i, ))

    else:
      print('Exception from', tasks[pid])
      print(status)

      processes[pid].join()

      if progress:
        progress_bars[pid].set_description('%s [error]' % (names[pid], ))

  if progress:
    for pb in progress_bars:
      try:
        pb.close()
      except:
        pass

if __name__ == '__main__':
  mp.set_start_method('spawn')
  import argparse

  from ope.experiment.master_list import available_models, available_models_tokenized, available_datasets
  from ope.experiment.master_list import ope_models, two_class_datasets, one_class_models, one_class_datasets

  import sys

  assert all([ 'tensorflow' not in m for m in sys.modules ])

  parser = argparse.ArgumentParser()
  parser.add_argument(
    '--datasets', nargs='+',
  )
  parser.add_argument(
    '--models', choices=['all'] + available_models_tokenized, nargs='+',
  )
  parser.add_argument(
    '--devices', '-d', required=True, type=int, nargs='+'
  )

  parser.add_argument(
    '--memory-fraction', '-mf', required=True, type=float
  )

  parser.add_argument(
    '--iterations', '-i', type=int, default=32
  )

  parser.add_argument(
    '--root', '-r', type=str, default='results/'
  )

  parser.add_argument(
    '--log-dir', '-l', type=str, default='logs/'
  )

  parser.add_argument(
    '--override', '-o', action='store_true', default=False
  )

  parser.add_argument(
    '--show-env', '-s', action='store_true', default=False
  )

  parser.add_argument(
    '--no-warmup', '-nw', action='store_true', default=False
  )

  parser.add_argument(
    '--no-progress', '-np', action='store_true', default=False
  )

  arguments = parser.parse_args()

  if len(arguments.devices) == 1 and arguments.devices[0] == -1:
    import os
    devices = [int(x) for x in os.environ['CUDA_VISIBLE_DEVICES'].split(',')]
  else:
    devices = arguments.devices

  if arguments.show_env:
    import os
    print('CUDA_VISIBLE_DEVICES=', os.environ.get('CUDA_VISIBLE_DEVICES', None))
    print('allowed devices=', devices)
    print('LD_LIBRARY_PATH=', os.environ.get('LD_LIBRARY_PATH', None))

    print(exec('nvidia-smi'))

  if 'all' in arguments.models:
    assert len(arguments.models) == 1
    models = available_models
  else:
    models = list(set([
      model
      for pattern in arguments.models
      for model in available_models
      if pattern in model
    ]))

  if 'all' in arguments.datasets:
    assert  len(arguments.datasets) == 1
    datasets = available_datasets
  else:
    datasets = list(set([
      dataset
      for pattern in arguments.datasets
      for dataset in available_datasets
      if dataset.startswith(pattern)
    ]))

  if arguments.memory_fraction < 0.0:
    memory_fraction = None
  else:
    memory_fraction = arguments.memory_fraction

  if not arguments.no_warmup:
    launch_warmup(datasets)

  tasks = [
    (dataset, model, arguments.iterations, arguments.root, arguments.log_dir, arguments.override)
    for model in models
    for dataset in datasets
  ]

  launch(devices, memory_fraction, tasks, not arguments.no_progress)