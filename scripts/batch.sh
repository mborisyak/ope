#!/bin/bash

ALL_METHODS="two-class semi-supervised brute-force-ope hmc-eope rmsprop-eope deep-eope nn-oc rae-oc deep-svdd-oc brute-force-oc hmc-eoc rmsprop-eoc deep-eoc"

if [ -z "$2" ]
then
  METHODS=$ALL_METHODS
else
  METHODS=$2
fi

echo $METHODS

for method in $METHODS
do
    sbatch --partition=gpu-1 --gres=gpu:1 --job-name="$1_$method" --error="$1_$method.err" --output="$1_$method.out" opt/ope/scripts/task.sh $1 $method &
done

wait
