import os
import gearup

import numpy as np
import tensorflow as tf

from craynn import *
from crayopt import *

from tqdm import tqdm

from ope.datasets import kdd

def uniform_onehot(rng, size, batch_size):
  indecies = rng.uniform(shape=(batch_size,), minval=0, maxval=size, dtype=tf.int32)
  return tf.one_hot(indecies, depth=size, dtype=tf.float32)

def get_uniform(feature_spec, batch_size, seed=111):
  rng = tf.random.Generator.from_seed(seed=seed)

  @tf.function(autograph=False)
  def generate():
    columns = list()
    for _, t, (min, max) in feature_spec:
      if t == 'continuous':
        column = rng.uniform(shape=(batch_size, 1), minval=0, maxval=1, dtype=tf.float32)
      elif t == 'binary':
        column = tf.cast(
          rng.uniform(shape=(batch_size, 1), minval=0, maxval=2, dtype=tf.int32),
          dtype=tf.float32
        )
      elif t == 'categorical':
        column = uniform_onehot(rng, size=max + 1, batch_size=batch_size)
      else:
        raise Exception('data type %s is not understood!' % (t,))

      columns.append(column)

    return tf.concat(columns, axis=1)

  return generate

def warmup(root: str = os.environ.get('DATA_ROOT', '.')):
  _ = kdd(seed=0, root=root)()

def main(
  groups: int = 0, samples_per_group: int = 1000,
  epsilon: float = 0.95, capacity: int = 48,
  seed: int = 111, repeat: int = 10,
  root: str = os.environ.get('DATA_ROOT', '.'),
  results: str = '.',
  progress: gearup.choice(none=None, tqdm=tqdm) = tqdm
):
  KDD = kdd(seed=seed, root=root)()

  uniform = get_uniform(feature_spec=KDD.feature_spec, batch_size=32, seed=seed + 1)

  n = capacity
  net = network((None, KDD.data_pos.shape[1]))(
    dense(4 * n),
    dense(3 * n),
    dense(2 * n),
    dense(n),
    dense(1, activation=linear()),
    flatten(1)
  )

  optimizer = tf_updates.adam()(net.variables(trainable=True))

  eps = tf.constant(epsilon, dtype=tf.float32)
  ceps = tf.constant(1 - epsilon, dtype=tf.float32)

  dataset_test = variable_dataset(KDD.data_test, KDD.labels_test)

  if groups > 0:
    pos_indx, neg_indx = KDD.subset(groups, samples_per_group)
    dataset_pos = variable_dataset(KDD.data_pos[pos_indx])
    dataset_neg = variable_dataset(KDD.data_neg[neg_indx])

    @tf.function(autograph=False)
    def step():
      X_pos, = dataset_pos.batch(32)
      X_neg, = dataset_neg.batch(32)
      X_gen = uniform()

      with optimizer:
        p_pos = net(X_pos)
        p_neg = net(X_neg)
        p_gen = net(X_gen)

        loss_pos = tf.nn.softplus(-p_pos)
        loss_gen = tf.nn.softplus(p_gen)
        loss_neg = tf.nn.softplus(p_neg)

        loss = tf.reduce_mean(loss_pos) + \
               eps * tf.reduce_mean(loss_neg) + \
               ceps * tf.reduce_mean(loss_gen) + \
               1e-4 * net.reg_l2()

        return optimizer(loss)

  else:
    dataset_pos = variable_dataset(KDD.data_pos)

    @tf.function(autograph=False)
    def step():
      X_pos, = dataset_pos.batch(32)
      X_gen = uniform()

      with optimizer:
        p_pos = net(X_pos)
        p_gen = net(X_gen)

        loss_pos = tf.nn.softplus(-p_pos)
        loss_gen = tf.nn.softplus(p_gen)

        loss = tf.reduce_mean(loss_pos) + \
               ceps * tf.reduce_mean(loss_gen) + \
               1e-4 * net.reg_l2()

        return optimizer(loss)

  losses = list()
  scores = list()

  for i in range(repeat):
    print('Experiment %d' % (i, ))
    net.reset()
    train.normalize_weights(net, dataset_pos[:1024], progress=progress)

    ls = train.iterate(step, n_iterations=len(dataset_pos) // 32, n_epoches=16, progress=progress)
    losses.append(ls)

    predictions, = dataset_test.eval(net, batch_size=1024)

    from sklearn.metrics import roc_auc_score
    score = roc_auc_score(KDD.labels_test, predictions)
    scores.append(score)

    print('  ROC AUC = %.10lf' % (score, ))

  path = os.path.join(
    results,
    'KDD-%d-%d-%d.json' % (capacity, groups, samples_per_group)
  )

  with open(path, 'w') as f:
    import json
    json.dump(scores, f)

if __name__ == '__main__':
  gearup.gearup(
    warmup=warmup,
    main=main
  )()