def main(root, target, min_iterations=0):
  from ope.experiment.utils import load_results

  import pickle

  summary = load_results(root, min_iterations=min_iterations)

  for dataset in summary:
    for model in summary[dataset]:
      print(dataset, model)

  with open(target, 'wb') as f:
    pickle.dump(summary, f)

if __name__ == '__main__':
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument(
    'root', type=str
  )

  parser.add_argument(
    'target', type=str
  )

  parser.add_argument(
    '--min-iterations', '-i', type=int, default=0,
  )

  arguments = parser.parse_args()

  main(arguments.root, arguments.target, arguments.min_iterations)