import numpy as np

### rae-oc is almost always either superior to or same as ae-oc
### ae-oc just reduces readability.
### same for nn-oc
exclude_methods = ['ae-oc', 'nn-oc']

model_labels = {
  'two-class' : 'cross-entropy',
  'semi-supervised' : 'semi-supervised',

  'rae-oc' : 'Robust AE',
  'deep-svdd-oc' : 'Deep SVDD',

  'brute-force-ope' : 'brute-force OPE',
  'hmc-eope' : 'HMC EOPE',
  'rmsprop-eope' : 'RMSProp EOPE',
  'deep-eope' : 'Deep EOPE',
}

dataset_order = [
  'HIGGS', 'SUSY', 'KDD',
  'MNIST', 'CIFAR', 'Omniglot'
]

dataset_desc = {
  'HIGGS' : 'Results on HIGGS dataset. The first row indicates numbers of negative samples used in training.',
  'SUSY' : 'Results on SUSY dataset. The first row indicates numbers of negative samples used in training.',
  'MNIST' : 'Results on MNIST dataset. The first row indicates numbers of original classes selected as negative class, '
            '10 images are sampled from each original class.',

  'CIFAR' : 'Results on CIFAR-10 dataset. The first row indicates numbers of original classes selected as negative class, '
            '10 images are sampled from each original class.',

  'Omniglot' : 'Results on Omniglot dataset. The first row indicates numbers of original classes selected as negative class, '
               '10 images are sampled from each original class. Greek, Braille and Futurama alphabets are used as normal classes.',

  'KDD' : 'Results on KDD-99 dataset. The first row indicates numbers of original classes selected as negative class, '
             'at most 1000 examples are sampled from each original class.',
}

def get_dimensions(results):
  results = expand_one_class(results)
  results = merge_methods(results)

  all_models = [
    model
    for model in one_class_models
    if model in results and model not in exclude_methods
  ] + [
    model
    for model in ope_models
    if model in results and model not in exclude_methods
  ]

  print(all_models)

  all_partitions = list(set([
    partition
    for model in results
    for partition in results[model]
  ]))

  all_partitions = sorted(all_partitions, key=lambda a: a[0] if a[0] is not None else -1)

  return results, all_models, all_partitions

def plot(path, results, format, w=9, h=6):
  import matplotlib.pyplot as plt

  plt.figure(figsize=(w, h))

  results, all_models, all_partitions = get_dimensions(results)

  xs = np.arange(len(all_partitions))
  offsets = np.linspace(-0.35, 0.35, num=len(all_models))
  bar_w = 0.7 / (len(all_models))

  for j, model in enumerate(all_models):
    means = np.zeros(len(all_partitions))
    stds = np.zeros(len(all_partitions))

    for i, p in enumerate(all_partitions):
      if p in results[model]:
        scores = results[model][p]
        means[i] = np.mean(scores) - 0.5
        stds[i] = np.std(scores, ddof=1)

    plt.bar(xs + offsets[j], means, bottom=0.5, width=bar_w, yerr=stds, label=model_labels.get(model, model))

  box = plt.gca().get_position()
  plt.gca().set_position([box.x0, box.y0 + 0.075 * box.height, box.width, 0.925 * box.height])

  plt.title(dataset)
  plt.legend(loc='bottom left', bbox_to_anchor=(0.95, -0.1), ncol=4, fontsize=9)
  plt.xticks(xs, [ 'one-class' if p[0] is None else str(p[0]) for p in all_partitions ])

  plt.ylabel('ROC AUC')
  plt.xlabel('fraction of negative samples')

  plt.savefig(path, format=format)
  plt.close()



def format_latext(rows, headers):
  header =  r'\hline & {header} \\ \hline'.format(
    header=' & '.join(headers)
  )

  body = '\n'.join([
    ' {row} \\\\ \\hline'.format(
      row=' & '.join(row)
    )
    for row in rows
  ])

  return '\\begin{tabular}{%s}\n%s\n%s\n\\end{tabular}' % (
    '|l' * (len(headers) + 1) + '|',
    header,
    body
  )

def format_value(m, s, best=False, format='txt'):
  if format == 'txt':
    if best:
      return '*%.3lf* +- %.3lf' % (m, s)
    else:
      return ' %.3lf  +- %.3lf' % (m, s)
  else:
    if best:
      return r'$\mathbf{%.3lf} \pm %.3lf$' % (m, s)
    else:
      return r'$%.3lf \pm %.3lf$' % (m, s)

def expand_one_class(results):
  from ope.experiment.master_list import oc_to_ope_mapping, ope_to_oc_mapping

  results = results.copy()

  all_partitions = list(set([
    partition
    for model in results
    for partition in results[model]
  ]))

  for model in results:
    if len(results[model]) == 1 and model in one_class_models and model not in oc_to_ope_mapping:
      one_class_partition = next(iter(results[model]))
      scores = results[model][one_class_partition]

      for partition in all_partitions:
        results[model][partition] = results[model].get(partition, scores)

  return results

def table(results, format='txt', fit=False):
  from ope.experiment.utils import argmax

  rows = []

  results, all_models, all_partitions = get_dimensions(results)

  header = [
    str(partition[0]) if partition[0] is not None else 'one class'
    for partition  in all_partitions
  ]

  best = {
    partition : argmax(all_models, [
      results[m].get(partition, None) for m in all_models
    ])
    for partition in all_partitions
  }

  for model in all_models:
    row = [model_labels.get(model, model)]

    for partition in all_partitions:

      if partition not in results[model]:
        row.append('-')
        continue

      scores = results[model][partition]
      row.append(
        format_value(np.mean(scores), np.std(scores), best=(model == best[partition]), format=format)
      )

    rows.append(row)
  if format == 'txt':
    from tabulate import tabulate
    return tabulate(rows, headers=header, disable_numparse=True)
  else:
    if fit:
      rows = [
        row[:6] for row in rows
      ]

      header = header[:5]

    return format_latext(rows, headers=header)

def merge_datasets(results):
  d = dict()

  for dataset in results:
    k = dataset.split('-')[0]
    if k not in d:
      d[k] = results[dataset]
    else:
      d[k] = merge_results(d[k], results[dataset])

  return d

if __name__ == '__main__':
  import os
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument('--root', type=str, default='results/')
  parser.add_argument('--summary', default=None)
  parser.add_argument('--outdir', type=str, default='tables/')
  parser.add_argument('--format', choices=['txt', 'latex', 'png', 'pdf'], type=str, default='txt')
  parser.add_argument('--merge',  action='store_true', default=False)
  parser.add_argument('--fit', action='store_true', default=False)

  args = parser.parse_args()

  root = args.root
  outdir = args.outdir
  format = args.format

  from ope.experiment import *
  from ope.experiment.utils import merge_methods, merge_results

  if args.summary is not None:
    root = args.summary

    if os.path.isdir(root):
      results = dict()
      for item in os.listdir(root):
        path = os.path.join(root, item)

        if os.path.isdir(path):
          continue

        import pickle
        with open(path, 'rb') as f:
          partial_results = pickle.load(f)

          if args.merge:
            partial_results = merge_datasets(partial_results)

          results = merge_results(results, partial_results)
    else:
      import pickle

      with open(root, 'rb') as f:
        results =  pickle.load(f)
  else:
    results = load_results(root)


  if args.merge:
    results = merge_datasets(results)

  sresults = {
    dataset: select_best_hyperparams(results[dataset])
    for dataset in dataset_order
    if dataset in results
  }

  for kd in results:
    print(kd)
    for km in results[kd]:
      print(kd, km)
      for part in results[kd][km]:
        print(part)
        print(results[kd][km][part])

  os.makedirs(outdir, exist_ok=True)

  if format in ['png', 'pdf']:
    for dataset in sresults:
      path = os.path.join(outdir, '%s.%s' % (dataset, format))
      plot(path, sresults[dataset], args.format)

  else:

    all_tables = []

    for dataset in sresults:
      t = table(sresults[dataset], args.format, args.fit)
      all_tables.append((dataset, t))

      path = os.path.join(outdir, '%s.%s' % (dataset, format))

      with open(path, 'w') as f:
        f.write(t)

    if format == 'txt':
      with open(os.path.join(outdir, 'all.txt'), 'w') as f:
        f.write('\n\n'.join(
          '== %s == \n%s\n' % (dataset, t)
          for dataset, t in all_tables
        ))
    else:
      here = os.path.dirname(__file__)

      from string import Template

      with open(os.path.join(here, 'template.latex')) as f:
        document_template = Template(f.read())

      source_path = os.path.join(outdir, 'all.latex')

      with open(source_path, 'w') as f:
        table_template = '\\begin{figure}[ptb]\n\\centering\n\\footnotesize\n%s\n\\caption{%s}\n\\label{tab:%s}\n\\end{figure}\n'

        f.write(
          document_template.substitute(
            body='\n\n'.join(
              table_template % (t, dataset_desc.get(dataset, 'Results on %s dataset.' % dataset), dataset.lower())
              for dataset, t in all_tables
            )
          )
        )

      try:
        import subprocess as sp
        import tempfile

        with tempfile.TemporaryDirectory() as tmp:
          sp.run([
            'cp', os.path.join(outdir, 'all.latex'), os.path.join(tmp, 'all.latex')
          ])

          sp.run([
            'xelatex', '-synctex=1', '-shell-escape', '-interaction=nonstopmode',
            os.path.join(tmp, 'all.latex')
          ], cwd=tmp)

          sp.run([
            'mv', os.path.join(tmp, 'all.pdf'), os.path.join(outdir, 'all.pdf')
          ])

      except:
        import traceback
        traceback.print_exc()
