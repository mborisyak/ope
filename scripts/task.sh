#!/bin/bash

srun python opt/ope/scripts/main.py --no-progress -i 128 -d -1 -mf 0.95 --datasets $1 --models $2