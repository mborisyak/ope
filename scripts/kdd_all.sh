#!/bin/bash

python scripts/kdd.py warmup

sbatch --gpus 1 -c 1 --job-name="OPE-0" --error="logs/OPE-0.err" --output="logs/OPE-0.out" scripts/kdd.sh \
  groups=0 repeat=10 seed=111 progress=none

sbatch --gpus 1 -c 1 --job-name="OPE-1" --error="logs/OPE-1.err" --output="logs/OPE-1.out" scripts/kdd.sh \
  groups=1 repeat=10 seed=111 progress=none

sbatch --gpus 1 -c 1 --job-name="OPE-2" --error="logs/OPE-2.err" --output="logs/OPE-2.out" scripts/kdd.sh \
  groups=2 repeat=10 seed=111 progress=none

sbatch --gpus 1 -c 1 --job-name="OPE-4" --error="logs/OPE-4.err" --output="logs/OPE-4.out" scripts/kdd.sh \
  groups=4 repeat=10 seed=111 progress=none

sbatch --gpus 1 -c 1 --job-name="OPE-8" --error="logs/OPE-8.err" --output="logs/OPE-8.out" scripts/kdd.sh \
  groups=8 repeat=10 seed=111 progress=none