__all__ = [
  'get_dense_network_pool'
]

def get_dense_network_pool(num_features, n, code_size, latent_size):
  from craynn import net

  from craynn import achain, select, concat
  from craynn import dense, flatten, batch_norm
  from craynn import default_nonlinearity, linear, softplus, gaussian
  from craynn import ones_init, zeros_init, constant_parameter

  b0 = constant_parameter(0, )
  bn = lambda activation=softplus(), beta=zeros_init(): batch_norm(
    axes=(0,),
    gamma=ones_init(), beta=beta,
    activation=activation
  )

  return {
    'classifier' : lambda: net((None, num_features))(
      dense(4 * n), dense(3 * n), dense(2 * n), dense(n),
      dense(1, activation=linear()), flatten(1)
    ),

    'inference': lambda: net((None, num_features))(
      dense(n), dense(2 * n), dense(3 * n), dense(4 * n),
      dense(latent_size, activation=linear()),
    ),

    'generator' : lambda: net((None, latent_size))(
      dense(4 * n, activation=linear()), bn(),
      dense(3 * n, activation=linear()), bn(),
      dense(2 * n, activation=linear()), bn(),
      dense(num_features, activation=linear()),
      bn(activation=linear(), beta=zeros_init())
    ),

    'encoder' : lambda: net((None, num_features))(
      dense(4 * n), dense(3 * n), dense(2 * n), dense(n),
      dense(code_size, activation=linear()),
    ),

    'decoder' : lambda: net((None, code_size))(
      dense(n), dense(2 * n), dense(3 * n), dense(4 * n),
      dense(num_features, activation=linear()),
    ),

    'dense_classifier' : lambda : net((None, code_size))(
      dense(2 * n), dense(n),
      dense(1, activation=linear()), flatten(1)
    ),

    'shallow_classifier': lambda: net((None, code_size))(
      dense(2 * n),
      dense(1, activation=linear()), flatten(1)
    ),

    'svdd' : lambda: net((None, num_features))(
      dense(4 * n, b=b0), dense(3 * n, b=b0), dense(2 * n, b=b0), dense(n, b=b0),
      dense(latent_size, activation=linear(), b=b0),
    ),
  }