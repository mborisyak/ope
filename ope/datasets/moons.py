import numpy as np

from .meta import *

__all__ = [
  'moons'
]

def _network_pool():
  from craynn import net

  from craynn import achain, select, concat
  from craynn import dense, flatten, batch_norm
  from craynn import default_nonlinearity, linear, softplus, gaussian
  from craynn import ones_init, zeros_init, constant_parameter

  b0 = constant_parameter(0, )
  bn = lambda activation=softplus(), beta=zeros_init(): batch_norm(
    axes=(0,),
    gamma=ones_init(), beta=beta,
    activation=activation
  )

  n = 32
  num_features = 2
  latent_size = 2
  code_size = 2

  return {
    'classifier': lambda: net((None, num_features))(
      dense(4 * n), dense(2 * n), dense(n),
      dense(1, activation=linear()), flatten(1)
    ),

    'rclassifier': lambda: net((None, num_features))(
      dense(4 * n, activation=gaussian()),
      dense(2 * n, activation=gaussian()),
      dense(n, activation=gaussian()),
      dense(1, activation=linear()), flatten(1)
    ),

    'biclassifier': lambda: net((None, num_features), (None, latent_size))(
      [achain(
        select[0],
        dense(4 * n),
        dense(3 * n),
      ),
        select[1]
      ],
      concat(),
      dense(1, activation=linear()), flatten(1)
    ),

    'inference': lambda: net((None, num_features))(
      dense(2 * n), dense(4 * n),
      dense(latent_size, activation=linear()),
    ),

    ### the same as inference but batch normalized
    'rev_generator': lambda: net((None, num_features))(
      dense(n, activation=linear()), bn(),
      dense(2 * n, activation=linear()), bn(),
      dense(3 * n, activation=linear()), bn(),
      dense(4 * n, activation=linear()), bn(),
      dense(latent_size, activation=linear()), bn(activation=linear(), beta=zeros_init())
    ),

    'generator': lambda: net((None, latent_size))(
      dense(4 * n, activation=linear()), bn(),
      dense(2 * n, activation=linear()), bn(),
      dense(num_features, activation=linear()), bn(activation=linear(), beta=zeros_init())
    ),

    'rgenerator': lambda: net((None, latent_size))(
      dense(4 * n, activation=linear()), bn(activation=gaussian()),
      dense(2 * n, activation=linear()), bn(activation=gaussian()),
      dense(num_features, activation=linear()), bn(activation=linear(), beta=zeros_init())
    ),

    'encoder': lambda: net((None, num_features))(
      dense(4 * n), dense(2 * n),
      dense(code_size, activation=linear()),
    ),

    'decoder': lambda: net((None, code_size))(
      dense(2 * n), dense(4 * n),
      dense(num_features, activation=linear()),
    ),

    'dense_classifier': lambda: net((None, code_size))(
      dense(2 * n), dense(n),
      dense(1, activation=linear()), flatten(1)
    ),

    'shallow_classifier': lambda: net((None, code_size))(
      dense(2 * n),
      dense(1, activation=linear()), flatten(1)
    ),

    'svdd': lambda: net((None, num_features))(
      dense(4 * n, b=b0), dense(2 * n, b=b0),
      dense(latent_size, activation=linear(), b=b0),
    ),
  }

class Moons(UngroupedDataset):
  def __init__(self, seed):
    from sklearn.datasets import make_moons

    get_pos = lambda size, seed: make_moons(n_samples=size, noise=0.05, random_state=seed)[0].astype('float32')

    data_pos = get_pos(64, seed)
    data_pos_test = get_pos(64, seed + 2)

    center = np.array([0.5, 0.25], dtype='float32')
    X_range = np.array([
      [-1.25, 2.25],
      [-0.75, 1.25],
    ], dtype='float32')

    self.bbox = X_range

    np.random.seed(seed + 3)

    get_neg = lambda n: np.concatenate([
        np.random.normal(size=(4 * n, 2)) / 4 + center[None, :],
        np.random.uniform(size=(n, 2)) * (X_range[:, 1] - X_range[:, 0])[None, :] + X_range[None, :, 0],
      ], axis=0).astype('float32')

    data_neg = get_neg(4)
    data_neg_test = get_neg(64)

    data_test = np.concatenate([
      data_pos_test,
      data_neg_test
    ], axis=0)

    labels_test = np.concatenate([
      np.ones(data_pos_test.shape[0], dtype='float32'),
      np.zeros(data_neg_test.shape[0], dtype='float32')
    ])

    super(Moons, self).__init__(
      data_pos, data_neg,
      data_test, labels_test,
      batch_size=32,
      seed=seed
    )

  def bounding_box(self):
    return self.bbox

  def network_pool(self):
    return _network_pool()

moons = lambda seed: lambda: Moons(seed)