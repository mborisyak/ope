import numpy as np

from .meta import *

__all__ = [
  'cifar10'
]

N = 16
LATENT_SIZE = 12 * N
CODE_SIZE = 12 * N

def _network_pool():
  from craynn import net
  from craynn import select, concat, achain

  from craynn import conv, dense, flatten, batch_norm
  from craynn import deconv, upscale, reshape

  from craynn import max_pool, global_max_pool
  from craynn import default_nonlinearity, linear
  from craynn import ones_init, zeros_init, constant_parameter

  b0 = constant_parameter(0)
  bn = lambda activation=default_nonlinearity: batch_norm(
    axes=(0,),
    gamma=ones_init(), beta=zeros_init(),
    activation=activation
  )

  return {
    'classifier' : lambda: net((None, 32, 32, 3))(
      conv(2 * N), conv(3 * N), max_pool(),
      conv(4 * N), conv(6 * N), max_pool(),
      conv(8 * N), conv(12 * N), global_max_pool(),
      dense(1, activation=linear()),
      flatten(1)
    ),

    'biclassifier': lambda: net((None, 32, 32, 3), (None, LATENT_SIZE))(
      [
        (
          select[0],
          conv(2 * N), conv(3 * N), max_pool(),
          conv(4 * N), conv(6 * N), max_pool(),
          conv(8 * N), conv(12 * N), global_max_pool(),
        ),
        select[1]
      ],

      concat(),
      dense(1, activation=linear()),
      flatten(1)
    ),

    'generator' : lambda: net((None, LATENT_SIZE))(
      reshape((None, 1, 1, LATENT_SIZE)),

      deconv(8 * N, activation=linear()), bn(),
      deconv(6 * N, activation=linear()), bn(),
      upscale(),

      deconv(4 * N, activation=linear()), bn(),
      deconv(3 * N, activation=linear()), bn(),
      upscale(),

      deconv(2 * N, activation=linear()), bn(),
      deconv(3, activation=linear()), bn(activation=linear()),
    ),

    'inference': lambda: net((None, 32, 32, 3))(
      conv(2 * N), conv(3 * N), max_pool(),
      conv(4 * N), conv(6 * N), max_pool(),
      conv(8 * N), conv(12 * N), global_max_pool(),
      dense(LATENT_SIZE, activation=linear()),
    ),

    'encoder' : lambda: net((None, 32, 32, 3))(
      conv(2 * N), conv(3 * N), max_pool(),
      conv(4 * N), conv(6 * N), max_pool(),
      conv(8 * N), conv(12 * N), global_max_pool(),
      dense(CODE_SIZE, activation=linear()),
    ),

    'decoder' : lambda: net((None, CODE_SIZE))(
      reshape((None, 1, 1, CODE_SIZE)),

      deconv(8 * N), deconv(6 * N), upscale(),
      deconv(4 * N), deconv(3 * N), upscale(),
      deconv(2 * N), deconv(3, activation=linear())
    ),

    'dense_classifier' : lambda : net((None, CODE_SIZE))(
      dense(8 * N), dense(4 * N), dense(1, activation=linear()), flatten(1)
    ),

    'shallow_classifier': lambda: net((None, CODE_SIZE))(
      dense(2 * N),
      dense(1, activation=linear()), flatten(1)
    ),

    'svdd' : lambda: net((None, 32, 32, 3))(
      conv(2 * N, b=b0), conv(3 * N, b=b0), max_pool(),
      conv(4 * N, b=b0), conv(6 * N, b=b0), max_pool(),
      conv(8 * N, b=b0), conv(12 * N, b=b0), global_max_pool(),
      dense(1, b=b0, activation=linear()),
      flatten(1)
    ),
}

class CIFAR10(GroupedDataset):
  def __init__(self, normal, seed, root=None):
    from craynn import datasets
    from craynn import utils

    self.normal = normal
    self.seed = seed

    data_train, labels_train, data_test, labels_test = datasets.cifar10(root=root)

    normal_indx = np.argmax(labels_train, axis=1) == normal
    data_pos = data_train[normal_indx]

    indx_neg, = np.where(np.argmax(labels_train, axis=1) != normal)
    data_neg = data_train[indx_neg]
    groups = np.argmax(labels_train, axis=1)[indx_neg]

    labels_test = np.where(
      np.argmax(labels_test, axis=1) == normal,
      1, 0
    ).astype('int32')

    super(CIFAR10, self).__init__(
      data_pos, data_neg,
      data_test, labels_test,
      batch_size=8,
      neg_group_indx=groups,
      seed=seed
    )

  def network_pool(self):
    return _network_pool()

  def name(self):
    return 'CIFAR-%d' % (self.normal, )

cifar10 = lambda normal, seed, root=None: lambda : CIFAR10(
  normal, seed=seed, root=root
)