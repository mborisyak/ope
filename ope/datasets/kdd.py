from .meta import *

__all__ = [
  'kdd'
]

class KDD(GroupedDataset):
  def __init__(self, seed, root=None):
    import numpy as np
    import crayflow as flow

    data_train, labels_train, data_test, labels_test, feature_spec, target_names = flow.dataflow(
      flow.instances.download_kdd_99('KDD-99/'),
      flow.stage(name='read')(
        flow.instances.read_kdd_99()
      ) @ flow.pickled('KDD-99/KDD-99.pickled')
    )(root=root)

    self.feature_spec = feature_spec
    self.target_names = target_names

    for i in range(data_train.shape[1]):
      if np.unique(data_train[:, i]).shape[0] > 2:
        data_train[:, i] = np.log1p(data_train[:, i])
        data_test[:, i] = np.log1p(data_test[:, i])

    data_train, data_test = flow.data.box(data_train, eps=1)(data_train, data_test)

    labels_train = np.argmax(labels_train, axis=1)
    neg_group_indx = labels_train[labels_train > 0]

    data_pos = data_train[labels_train == 0]
    data_neg = data_train[labels_train > 0]

    labels_test = np.where(np.argmax(labels_test, axis=1) == 0, 1, 0)

    super(KDD, self).__init__(
      data_pos, data_neg,
      data_test, labels_test,
      neg_group_indx=neg_group_indx,
      batch_size=32,
      seed=seed
    )

  def network_pool(self):
    from .common import get_dense_network_pool

    return get_dense_network_pool(
      num_features=123,
      n=48,
      code_size=62,
      latent_size=96
    )

kdd = lambda seed, root=None: lambda: KDD(seed, root)