import numpy as np

from .meta import *

__all__ = [
  'mnist'
]

N = 16
LATENT_SIZE = 8 * N
CODE_SIZE = 8 * N

def _network_pool():
  from craynn import net

  from craynn import select, concat, achain

  from craynn import conv, dense, flatten, batch_norm
  from craynn import deconv, upscale, reshape

  from craynn import max_pool, global_max_pool
  from craynn import default_nonlinearity, linear, gaussian
  from craynn import ones_init, zeros_init, constant_parameter

  b0 = constant_parameter(0)
  bn = lambda activation=default_nonlinearity: batch_norm(
    axes=(0,),
    gamma=ones_init(), beta=zeros_init(),
    activation=activation
  )

  return {
    'classifier' : lambda: net((None, 28, 28, 1))(
      conv(2 * N), conv(3 * N), max_pool(),
      conv(4 * N), conv(6 * N), max_pool(),
      conv(8 * N), global_max_pool(),
      dense(1, activation=linear()),
      flatten(1)
    ),

    'rclassifier': lambda: net((None, 28, 28, 1))(
      conv(2 * N, activation=gaussian()), conv(3 * N, activation=gaussian()), max_pool(),
      conv(4 * N, activation=gaussian()), conv(6 * N, activation=gaussian()), max_pool(),
      conv(8 * N, activation=gaussian()), global_max_pool(),
      dense(1, activation=linear()),
      flatten(1)
    ),

    'biclassifier' : lambda : net((None, 28, 28, 1), (None, LATENT_SIZE))(
      [
        (
          select[0],
          conv(2 * N), conv(3 * N), max_pool(),
          conv(4 * N), conv(6 * N), max_pool(),
          conv(8 * N), global_max_pool(),
       ),
        select[1]
      ],

      concat(),
      dense(1, activation=linear()),
      flatten(1)
    ),

    'generator' : lambda: net((None, LATENT_SIZE))(
      reshape((None, 1, 1, LATENT_SIZE)), upscale(),

      deconv(6 * N, activation=linear()), bn(),
      upscale(),

      deconv(4 * N, activation=linear()), bn(),
      deconv(3 * N, activation=linear()), bn(),
      upscale(),

      deconv(2 * N, activation=linear()), bn(),
      deconv(1, activation=linear()), bn(activation=linear()),
    ),

    'inference': lambda: net((None, 28, 28, 1))(
      conv(2 * N), conv(3 * N), max_pool(),
      conv(4 * N), conv(6 * N), max_pool(),
      conv(8 * N), global_max_pool(),
      dense(LATENT_SIZE, activation=linear()),
    ),

    'encoder' : lambda: net((None, 28, 28, 1))(
      conv(2 * N), conv(3 * N), max_pool(),
      conv(4 * N), conv(6 * N), max_pool(),
      conv(8 * N), global_max_pool(),
      dense(CODE_SIZE, activation=linear()),
    ),

    'decoder' : lambda: net((None, CODE_SIZE))(
      reshape((None, 1, 1, CODE_SIZE)), upscale(),

      deconv(6 * N), upscale(),
      deconv(4 * N), deconv(3 * N), upscale(),
      deconv(2 * N), deconv(1, activation=linear())
    ),

    'dense_classifier' : lambda : net((None, CODE_SIZE))(
      dense(4 * N), dense(2 * N),
      dense(1, activation=linear()), flatten(1)
    ),

    'svdd' : lambda : net((None, 28, 28, 1))(
      conv(2 * N, b=b0), conv(3 * N, b=b0), max_pool(),
      conv(4 * N, b=b0), conv(6 * N, b=b0), max_pool(),
      conv(8 * N, b=b0), global_max_pool(),
      dense(1, b=b0, activation=linear()),
      flatten(1)
    ),

    'shallow_classifier': lambda: net((None, CODE_SIZE))(
      dense(2 * N),
      dense(1, activation=linear()), flatten(1)
    )
  }

class MNIST(GroupedDataset):
  def __init__(self, normal, seed, root=None):
    from craynn import datasets

    self.normal = normal

    data_train, labels_train, data_test, labels_test = datasets.mnist(root=root)

    normal_indx = np.argmax(labels_train, axis=1) == normal
    data_pos = data_train[normal_indx]

    indx_neg, = np.where(np.argmax(labels_train, axis=1) != normal)
    data_neg = data_train[indx_neg]
    groups = np.argmax(labels_train, axis=1)[indx_neg]

    labels_test = np.where(
      np.argmax(labels_test, axis=1) == normal,
      1, 0
    ).astype('int32')

    super(MNIST, self).__init__(
      data_pos, data_neg,
      data_test, labels_test,
      batch_size=8,
      neg_group_indx=groups,
      seed=seed
    )

  def network_pool(self):
    return _network_pool()

  def name(self):
    return 'MNIST-%d' % (self.normal, )

mnist = lambda normal, seed, root=None: lambda: MNIST(
  normal, seed=seed, root=root
)