from .meta import *

__all__ = [
  'susy'
]

class SUSY(UngroupedDataset):
  def __init__(self, seed, root=None):
    from craynn import utils
    from craynn import datasets

    data, labels = datasets.dataflow(
      datasets.download_susy('SUSY/'),
      datasets.read_susy @ datasets.pickled('SUSY/susy.pickled')
    )(root)

    data_train, labels_train, data_test, labels_test = utils.split(data, labels, split_ratios=(10, 1), seed=seed)

    data_pos = data_train[labels_train == 1]
    data_neg = data_train[labels_train == 0]

    super(SUSY, self).__init__(
      data_pos, data_neg,
      data_test, labels_test,
      batch_size=32,
      seed=seed
    )

  def network_pool(self):
    from .common import get_dense_network_pool

    return get_dense_network_pool(
      num_features=18,
      n=96,
      code_size=9,
      latent_size=64
    )

susy = lambda seed, root=None: lambda: SUSY(seed, root)