import numpy as np

__all__ = [
  'UngroupedDataset',
  'GroupedDataset'
]

def random_subindx(size, num_samples, rng):
  perm = rng.permutation(size)
  return perm[:num_samples]

class Dataset(object):
  def __init__(
    self,
    data_pos, data_neg,
    data_test, labels_test,
    batch_size, seed
  ):
    self.data_pos = data_pos
    self.data_neg = data_neg

    self.data_test = data_test
    self.labels_test = labels_test

    self.batch_size = batch_size
    self.rng = np.random.RandomState(seed=seed)

  def bounding_box(self):
    shape = self.data_pos.shape[1:]
    return np.stack([
      np.zeros(shape=shape, dtype=self.data_pos.dtype),
      np.ones(shape=shape, dtype=self.data_pos.dtype),
    ], axis=len(shape))

  def subset(self, *args):
    raise NotImplementedError

  def network_pool(self):
    raise NotImplementedError

  def name(self):
    return self.__class__.__name__

class UngroupedDataset(Dataset):
  def __init__(
    self,
    data_pos, data_neg, data_test, labels_test,
    batch_size, seed
  ):
    super(UngroupedDataset, self).__init__(
      data_pos, data_neg,
      data_test, labels_test,
      batch_size, seed=seed
    )

  def subset(self, num_samples):
    if num_samples is None:
      return slice(None, None, None), None
    else:
      indx = random_subindx(self.data_neg.shape[0], num_samples, self.rng)
      return slice(None, None, None), indx

  def network_pool(self):
    raise NotImplementedError

def random_group(group_indx, group_id, samples_per_group, rng):
  indx, = np.where(group_indx == group_id)
  num_samples = min([ indx.shape[0], samples_per_group ])

  return rng.choice(
    indx,
    size=num_samples,
    replace=False
  )

def random_groups(group_indx, num, samples_per_group, rng):
  ugroups = np.unique(group_indx)
  selected_groups = rng.choice(ugroups, size=num, replace=False)

  indx = np.concatenate([
    random_group(group_indx, group, samples_per_group, rng)
    for group in selected_groups
  ], axis=0)

  result = indx, selected_groups

  return result


class GroupedDataset(Dataset):
  def __init__(
    self,
    data_pos, data_neg, data_test, labels_test,
    batch_size, neg_group_indx, seed
  ):
    super(GroupedDataset, self).__init__(
      data_pos, data_neg, data_test, labels_test,
      batch_size, seed=seed
    )

    self.neg_groups = neg_group_indx

  def subset(self, num_groups, samples_per_group):
    if num_groups is None:
      return slice(None, None, None), None
    else:
      indx, _ = random_groups(self.neg_groups, num_groups, samples_per_group, self.rng)
      return slice(None, None, None), indx

  def network_pool(self):
    raise NotImplementedError
