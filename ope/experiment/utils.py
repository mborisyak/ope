import os
import pickle

import numpy as np

from .task import TaskState

__all__ = [
  'load_results',
  'bind',
  'select_best_hyperparams',

  'argmax',
  'merge_methods',
  'merge_results'
]

def amerge(d1, d2, level=0, max_level=None):
  if max_level is not None and level >= max_level:
    return d1

  if isinstance(d1, dict) and isinstance(d2, dict):
    d = d1.copy()

    for k in d2:
      if k in d:
        d[k] = amerge(d[k], d2[k], level=level + 1, max_level=max_level)
      else:
        d[k] = d2[k]

    return d
  elif isinstance(d1, (list, tuple)) and isinstance(d2, (list, tuple)):
    return d1 + d2
  else:
    raise ValueError('Can not merge %s and %s' % (d1, d2))

def merge_results(d1, d2):
 return amerge(d1, d2)


def bind(f, values):
  import inspect

  signature = inspect.signature(f)
  args = dict()

  for name in signature.parameters:
    args[name] = values[name]()

  return args

def walk(root):
  for item in os.listdir(root):
    path = os.path.join(root, item)

    if os.path.isdir(path):
      for subitem in walk(path):
        yield (item,) + subitem
    else:
      yield (item,)

import re
version_regexp = re.compile('^(\d+).pickled$')

def get_version(item):
  match = version_regexp.fullmatch(item)
  return int(match.groups()[0])

def load_results(root, min_iterations=32):
  index = dict()

  for item in walk(root):
    try:
      dataset, partition, model, repeat, f = item
    except:
      print('Failed to process %s' % (item, ))
      continue

    key = (dataset, partition, model, repeat)

    version = get_version(f)
    path = os.path.join(root, *key, f)

    if version < min_iterations:
      continue

    if key not in index:
      index[key] = (version, path)
    else:
      previous_version, _ = index[key]
      if previous_version < version:
        index[key] = (version, path)

  results = dict()
  for key in index:
    version, path = index[key]
    try:
      with open(path, 'rb') as f:
        state = pickle.load(f)
        assert isinstance(state, TaskState)
    except:
      import traceback
      traceback.print_exc()
      print('Failed to load %s' % (path, ))

      continue

    dataset = state.dataset
    partition = state.partition
    model = state.model
    hyperparameters = tuple(state.hyperparameters.items())

    if dataset not in results:
      results[dataset] = dict()

    if model not in results[dataset]:
      results[dataset][model] = dict()

    if partition not in results[dataset][model]:
      results[dataset][model][partition] = dict()

    if hyperparameters not in results[dataset][model][partition]:
      results[dataset][model][partition][hyperparameters] = list()

    results[dataset][model][partition][hyperparameters].append(
      (state.val_scores, state.test_scores)
    )

  return results

def select_best_hyperparams(results):
  selected = dict()

  for model in results:
    selected[model] = dict()

    for partition in results[model]:
      d = results[model][partition]
      params = [ p for p in d ]

      if any([ val_score is None for p in params for val_score, test_score in d[p] ]):
        selected[model][partition] = [
          test_score
          for param in params
          for val_score, test_score in d[param]
        ]
      else:
        val_scores = [
          np.mean([val_score for val_score, test_score in d[p]])
          for p in params
        ]

        best = int(np.argmax(val_scores))

        selected[model][partition] = [
          test_score
          for val_score, test_score in d[params[best]]
        ]

  return selected

def merge_methods(results):
  from ope.experiment.master_list import ope_to_oc_mapping, oc_to_ope_mapping

  merged = dict()

  for model in results:
    if model in oc_to_ope_mapping:
      if oc_to_ope_mapping[model] in results:
        continue
      else:
        merged[model] = results[model]
        import warnings
        warnings.warn('Missing ope model for %s' % (model,))
        continue


    merged[model] = results[model].copy()

    if model in ope_to_oc_mapping:

      companion_model = ope_to_oc_mapping[model]

      if companion_model not in results:
        companion_model = companion_model.replace('eoc', 'oc')

      if companion_model not in results:
        import warnings
        warnings.warn('Missing one class model for %s' % (model, ))
        continue

      assert len(results[companion_model].keys()) == 1, (
        companion_model,
        list(results[companion_model].keys())
      )

      oc_partition, = results[companion_model].keys()

      merged[model][oc_partition] = results[companion_model][oc_partition]

  return merged


def argmax(keys, values):
  max = 0
  arg = None

  for k, v in zip(keys, values):
    if v is None:
      continue

    if np.mean(v) > max:
      max = np.mean(v)
      arg = k

  return arg
