from .meta import *
from .utils import *


__all__ = [
  'brute_force_ope',
  'brute_force_rope',

  'brute_force_oc',
]

class BruteForceOneClass(Model):
  def __init__(self, classifier, X_pos, bounding_box, eps=0.9):
    import tensorflow as tf
    from craynn.utils import tensor_shape

    num_pseudo = tensor_shape(X_pos)[0]

    self.classifier = classifier
    self.generator = uniform_generator(num_pseudo, bounding_box)
    self.X_pseudo = self.generator()

    ### substituting X_neg by X_pseudo
    self.X, self.y, self.w = combine(X_pos, X_neg=self.X_pseudo, alpha=(1 - eps))

    self.predictions, self.loss, self.gradients = logit_binary_crossentropy(
      classifier, self.X, self.y, weights=self.w
    )
    
    super(BruteForceOneClass, self).__init__(eps=eps)

  def __call__(self, X):
    return self.classifier(X)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf
    return tf.no_op()

  def updates(self):
    import tensorflow as tf
    return tf.no_op()

brute_force_oc = lambda eps=0.9: lambda classifier: \
    lambda X_pos, X_neg, alpha, bounding_box: BruteForceOneClass(
      classifier, X_pos, eps=eps, bounding_box=bounding_box
    )

class BruteForceOPE(Model):
  def __init__(self, classifier, X_pos, X_neg, bounding_box, alpha=0.5, eps=0.9):
    from craynn.utils import tensor_shape
    num_pseudo = tensor_shape(X_pos)[0]

    self.classifier = classifier

    self.generator = uniform_generator(num_pseudo, bounding_box)
    self.X_pseudo = self.generator()

    self.X, self.y, self.w = combine(X_pos, X_neg=X_neg, alpha=alpha, X_pseudo=self.X_pseudo, eps=eps)

    self.predictions, self.loss, self.gradients = logit_binary_crossentropy(
      classifier, self.X, self.y, weights=self.w
    )
    
    super(BruteForceOPE, self).__init__(alpha=alpha, eps=eps)

  def __call__(self, X):
    return self.classifier(X)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf
    return tf.no_op()

  def updates(self):
    import tensorflow as tf
    return tf.no_op()

brute_force_ope = lambda eps=0.9: lambda classifier: \
    lambda X_pos, X_neg, alpha, bounding_box: BruteForceOPE(
      classifier, X_pos, X_neg,
      alpha=alpha, eps=eps, bounding_box=bounding_box
    )

brute_force_rope = lambda eps=0.9: lambda rclassifier: \
    lambda X_pos, X_neg, alpha, bounding_box: BruteForceOPE(
      rclassifier, X_pos, X_neg,
      alpha=alpha, eps=eps, bounding_box=bounding_box
    )