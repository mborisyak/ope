from .meta import Model

__all__ = [
  'deep_energy_ope',
  'deep_energy_rope',
  'cycle_deep_energy_ope',
  'deep_energy_oc',

  'bideep_energy_ope',
  'bideep_energy_oc'
]

class DeepEnergy(Model):
  def __init__(self, classifier, generator, X_pos, X_neg, alpha=None, eps=0.9):
    import tensorflow as tf
    from craynn.layers import get_output_shape
    from craynn.utils import tensor_shape

    from craynn.updates import tf_updates

    self.classifier = classifier
    self.generator = generator

    assert len(generator.inputs) == 1
    latent_shape = get_output_shape(generator.inputs[0])

    num_pseudo = tensor_shape(X_pos)[0]

    self.generator_latent = tf.random_normal(
      shape=(num_pseudo, ) + latent_shape[1:],
      dtype='float32'
    )

    self.X_pseudo, = generator(self.generator_latent)

    bn_scale = generator.variables(normalization_scales=True)
    assert len(bn_scale) > 0

    pseudo_entropy = sum([
      tf.reduce_sum(tf.log(gamma ** 2))
      for gamma in bn_scale
    ])

    self.p_pseudo, = classifier(self.X_pseudo)

    self.loss_generator = -tf.reduce_mean(self.p_pseudo) - 0.5 * pseudo_entropy

    self.p_pos, = classifier(X_pos)

    self.loss_pos = tf.reduce_mean(tf.nn.softplus(-self.p_pos))
    self.loss_pseudo = tf.reduce_mean(self.p_pseudo)

    c_pseudo = tf.constant(1 - eps, dtype='float32')
    reg = 1e-2 * tf.reduce_mean(self.p_pseudo ** 2)

    if alpha is not None:
      self.p_neg, = classifier(X_neg)

      self.loss_neg = tf.reduce_mean(tf.nn.softplus(self.p_neg))

      c_neg = tf.constant(alpha, dtype='float32')

      self.loss = self.loss_pos + c_neg * self.loss_neg + c_pseudo * (self.loss_pseudo + reg)
    else:
      self.loss = self.loss_pos + c_pseudo * self.loss_pseudo + reg
    ###  list(zip(
    ### generator.variables(),
    ### tf.gradients(self.loss_generator, generator.variables())
    ### )) +

    self.gradients = list(zip(
      classifier.variables(),
      tf.gradients(
        self.loss, classifier.variables(),
        stop_gradients=[X_pos, self.X_pseudo] + ([] if alpha is None else [X_neg])
      )
    ))

    self.gen_optimizer = tf_updates.adam(learning_rate=1e-3)(self.loss_generator, generator.variables())
    
    super(DeepEnergy, self).__init__(alpha=alpha, eps=eps)

  def __call__(self, X):
    return self.classifier(X)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf
    return self.gen_optimizer.reset()

  def updates(self):
    import tensorflow as tf
    return self.gen_optimizer.updates()

deep_energy_ope = lambda eps=0.9: lambda classifier, generator: lambda X_pos, X_neg, alpha, bounding_box: \
  DeepEnergy(classifier, generator, X_pos, X_neg, alpha=alpha, eps=eps)

deep_energy_rope = lambda eps=0.9: lambda rclassifier, rgenerator: lambda X_pos, X_neg, alpha, bounding_box: \
  DeepEnergy(rclassifier, rgenerator, X_pos, X_neg, alpha=alpha, eps=eps)

deep_energy_oc = lambda eps=0.9: lambda classifier, generator: lambda X_pos, X_neg, alpha, bounding_box: \
  DeepEnergy(classifier, generator, X_pos, X_neg, alpha=None, eps=eps)

class CycleDeepEnergy(Model):
  def __init__(self, biclassifier, gen1, gen2, inf1, inf2, X_pos, X_neg, alpha=None, eps=0.9, cycle_coef=1e-1):
    import tensorflow as tf
    from craynn.layers import get_output_shape
    from craynn.utils import tensor_shape
    from craynn.updates import tf_updates

    self.classifier = biclassifier
    self.gen1 = gen1
    self.gen2 = gen2
    self.inf1 = inf1
    self.inf2 = inf2

    Z_shape = get_output_shape(gen1.inputs[0])
    latent_shape = get_output_shape(gen1.outputs[0])
    num_pseudo = tensor_shape(X_pos)[0]

    self.Z = tf.random_normal(
      shape=(num_pseudo,) + Z_shape[1:],
      dtype='float32'
    )

    bn_scale = \
      gen1.variables(normalization_scales=True) + gen2.variables(normalization_scales=True) + \
      inf1.variables(normalization_scales=True) + inf2.variables(normalization_scales=True)

    X_latent, = gen1(self.Z)
    X_gen, = gen2(X_latent)

    self.X_pseudo = X_gen

    Z_latent, = inf1(X_pos)
    Z_inf, = inf2(Z_latent)

    Z_cycled, = inf2(X_latent)
    X_cycled, = gen2(Z_latent)

    e_gen, = biclassifier(X_gen, self.Z)
    e_pos, = biclassifier(X_pos, Z_inf)

    Z_cycle_loss = tf.reduce_mean((Z_cycled - self.Z) ** 2)
    X_cycle_loss = tf.reduce_mean((X_cycled - X_pos) ** 2)

    pseudo_entropy = sum([
      tf.reduce_sum(tf.log(gamma ** 2))
      for gamma in bn_scale
    ])

    c_pseudo = tf.constant(1 - eps, dtype='float32')

    reg = 1e-2 * tf.reduce_mean(e_gen ** 2)

    if alpha is not None:
      Z_latent_neg, = inf1(X_neg)
      Z_inf_neg, = inf2(Z_latent_neg)

      e_neg, = biclassifier(X_neg, Z_inf_neg)

      c_neg = tf.constant(alpha, dtype='float32')

      self.loss_classifier = \
        tf.reduce_mean(tf.nn.softplus(-e_pos)) + \
        c_neg * tf.reduce_mean(tf.nn.softplus(e_neg)) + \
        c_pseudo * tf.reduce_mean(e_gen) + \
        reg
    else:
      self.loss_classifier = \
        tf.reduce_mean(tf.nn.softplus(-e_pos)) + \
        c_pseudo * tf.reduce_mean(e_gen) + \
        reg

    c_coef = tf.constant(cycle_coef, dtype='float32')
    self.loss_generator = \
      -tf.reduce_mean(e_gen)\
      - tf.reduce_mean(e_pos) \
      - 0.5 * pseudo_entropy \
      + c_coef * (X_cycle_loss + Z_cycle_loss)

    generator_variables = gen1.variables() + gen2.variables() + inf1.variables() + inf2.variables()

    ###list(zip(
    #   generator_variables,
    #   tf.gradients(
    #     self.loss_generator,
    #     generator_variables,
    #   )
    # ))

    self.gradients = list(zip(
      biclassifier.variables(),
      tf.gradients(
        self.loss_classifier,
        biclassifier.variables(),
        stop_gradients=[X_gen, Z_inf]
      )
    ))

    self.gen_optimizer = tf_updates.adam(1e-3)(self.loss_generator, generator_variables)
    
    super(CycleDeepEnergy, self).__init__(eps=eps, alpha=alpha, cycle_coef=cycle_coef)

  def __call__(self, X):
    Z_latent, = self.inf1(X)
    Z_inf, = self.inf2(Z_latent)

    return self.classifier(X, Z_inf)[0]

  def loss_and_grads(self):
    return self.loss_classifier, self.gradients

  def reset(self):
    import tensorflow as tf
    return self.gen_optimizer.reset()

  def updates(self):
    import tensorflow as tf
    return self.gen_optimizer.updates()

cycle_deep_energy_ope = lambda eps=0.9, cycle_coef=0.1: \
  lambda biclassifier, gen1, gen2, inf1, inf2: lambda X_pos, X_neg, alpha, bounding_box: \
    CycleDeepEnergy(
      biclassifier, gen1, gen2, inf1, inf2,
      X_pos, X_neg,
      alpha=alpha, eps=eps, cycle_coef=cycle_coef
    )


class BiDeepEnergy(Model):
  def __init__(self, biclassifier, generator, inference, X_pos, X_neg, alpha=None, eps=0.9):
    import numpy as np
    import tensorflow as tf
    from craynn import glorot_scaling
    from craynn.layers import get_output_shape
    from craynn.utils import tensor_shape

    self.classifier = biclassifier
    self.generator = generator
    self.inference = inference

    assert len(generator.inputs) == 1
    latent_shape = get_output_shape(generator.inputs[0])
    num_pseudo = tensor_shape(X_pos)[0]

    self.true_latent = tf.random_normal(
      shape=(num_pseudo,) + latent_shape[1:],
      dtype='float32'
    )

    self.X_pseudo, = generator(self.true_latent)

    self.inferred_pos, = inference(X_pos)

    if alpha is not None:
      self.inferred_neg, = inference(X_neg)
    else:
      self.inferred_neg = None

    self.energy_gen, = biclassifier(self.X_pseudo, self.true_latent)
    self.energy_pos, = biclassifier(X_pos, self.inferred_pos)
    if alpha is not None:
      self.energy_neg, = biclassifier(X_neg, self.inferred_neg)
    else:
      self.energy_neg = None

    pseudo_entropy_generator = 0.5 * sum([
      tf.reduce_sum(tf.log(gamma ** 2))
      for gamma in generator.variables(normalization_scales=True)
    ])

    pseudo_entropy_inference = 0.5 * sum([
      tf.reduce_sum(tf.log(gamma ** 2))
      for gamma in inference.variables(normalization_scales=True)
    ])

    self.loss_generator = -tf.reduce_mean(self.energy_gen) - 0.5 * pseudo_entropy_generator
    self.loss_inference = -tf.reduce_mean(self.energy_pos) - 0.5 * pseudo_entropy_inference

    self.loss_pos = tf.reduce_mean(tf.nn.softplus(-self.energy_pos))
    self.loss_pseudo = tf.reduce_mean(self.energy_gen)
    if alpha is not None:
      self.loss_neg = tf.reduce_mean(tf.nn.softplus(self.energy_neg))
    else:
      self.loss_neg = None

    c_pseudo = tf.constant(1 - eps, dtype='float32')

    reg = 1e-2  * tf.reduce_mean(self.energy_gen ** 2)

    if alpha is not None:
      c_neg = tf.constant(alpha, dtype='float32')

      self.loss = self.loss_pos + c_neg * self.loss_neg + c_pseudo * self.loss_pseudo + reg
    else:
      self.loss = self.loss_pos + c_pseudo * self.loss_pseudo + reg

    self.gradients = list(zip(
      generator.variables(),
      tf.gradients(
        self.loss_generator,
        generator.variables(),
      )
    )) + list(zip(
      inference.variables(),
      tf.gradients(
        self.loss_inference,
        inference.variables(),
      )
    )) + list(zip(
      biclassifier.variables(),
      tf.gradients(
        self.loss,
        biclassifier.variables(),
        stop_gradients=[self.X_pseudo, self.inferred_pos]  + ([] if alpha is None else [self.inferred_neg])
      )
    ))

    super(BiDeepEnergy, self).__init__(eps=eps, alpha=alpha)

  def __call__(self, X):
    inferred, = self.inference(X)
    return self.classifier(X, inferred)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf
    return tf.no_op()

  def updates(self):
    import tensorflow as tf
    return tf.no_op()


bideep_energy_ope = lambda eps=0.9: \
  lambda biclassifier, generator, rev_generator: lambda X_pos, X_neg, alpha, bounding_box: \
    BiDeepEnergy(
      biclassifier, generator, rev_generator, X_pos, X_neg,
      alpha=alpha, eps=eps
    )

bideep_energy_oc = lambda eps=0.9: \
  lambda biclassifier, generator, rev_generator: lambda X_pos, X_neg, alpha, bounding_box: \
    BiDeepEnergy(
      biclassifier, generator, rev_generator, X_pos, X_neg,
      alpha=None, eps=eps
    )