from .meta import Model
from .utils import *


__all__ = [
  'semi_supervised',
  'ae_oc'
]


class SemiSupervised(Model):
  def __init__(self, classifier, encoder, decoder, X_pos, X_neg, alpha=1):
    import tensorflow as tf
    from craynn.layers import get_output_shape

    self.encoder = encoder
    self.decoder = decoder
    self.classifier = classifier

    shape = get_output_shape(self.encoder.inputs[0])[1:]

    self.X, self.y, self.w = combine(X_pos, X_neg, alpha=alpha)

    self.X_code, = encoder(self.X)
    self.X_rec, = decoder(self.X_code)
    self.predictions, = classifier(self.X_code)

    loss_rec = tf.reduce_mean(
      self.w * tf.reduce_mean(
        (self.X_rec - self.X) ** 2, axis=list(range(1, len(shape) + 1))
      )
    )

    losses = \
      self.y * tf.nn.softplus(-self.predictions) + (1 - self.y) * tf.nn.softplus(self.predictions)

    self.loss = tf.reduce_sum(self.w * losses)

    ae_vars = encoder.variables() + decoder.variables()

    self.gradients = list(zip(
      ae_vars,
      tf.gradients(
        loss_rec, ae_vars,
        stop_gradients=[self.X, self.y, self.w]
      )
    )) + list(zip(
      classifier.variables(),
      tf.gradients(
        self.loss, classifier.variables(),
        stop_gradients=[self.X, self.y, self.w]
      )
    ))
    
    super(SemiSupervised, self).__init__(alpha=alpha)

  def __call__(self, X):
    code, = self.encoder(X)
    return self.classifier(code)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf

    return tf.no_op()

  def updates(self):
    import tensorflow as tf

    return tf.no_op()

semi_supervised = lambda : lambda dense_classifier, encoder, decoder: lambda X_pos, X_neg, alpha, bounding_box: \
  SemiSupervised(dense_classifier, encoder, decoder, X_pos, X_neg, alpha=alpha)

class AEOneClass(Model):
  def __init__(self, encoder, decoder, X_pos):
    import tensorflow as tf
    from craynn.utils import tensor_shape

    self.shape = tensor_shape(X_pos)

    self.encoder = encoder
    self.decoder = decoder

    self.code, = encoder(X_pos)
    self.reconstructed, = decoder(self.code)

    self.loss = tf.reduce_mean(
      (X_pos - self.reconstructed) ** 2
    )

    ae_vars = encoder.variables() + decoder.variables()

    self.gradients = list(zip(
      ae_vars,
      tf.gradients(self.loss, ae_vars)
    ))
    
    super(AEOneClass, self).__init__()

  def loss_and_grads(self):
    return self.loss, self.gradients

  def __call__(self, X):
    import tensorflow as tf

    code, = self.encoder(X)
    rec, = self.decoder(code)
    return -tf.log(
      tf.reduce_mean(
        (X - rec) ** 2,
        axis=list(range(1, len(self.shape)))
      ) + 1e-3
    )

  def updates(self):
    import tensorflow as tf

    return tf.no_op()

  def reset(self):
    import tensorflow as tf

    return tf.no_op()

ae_oc = lambda : lambda encoder, decoder: lambda X_pos, X_neg, alpha, bounding_box: \
  AEOneClass(encoder, decoder, X_pos)