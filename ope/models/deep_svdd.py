from .meta import Model

__all__ = [
  'deep_svdd',
  'deep_svdd_oc'
]

class DeepSVDD(Model):
  def __init__(self, inference, X_pos, X_neg, reg_coef=1, alpha=None):
    import numpy as np
    import tensorflow as tf
    from craynn import glorot_scaling
    from craynn.layers import get_output_shape
    from .utils import combine

    if alpha is None:
      X, w = X_pos, None
    else:
      X, _, w = combine(X_pos, X_neg, alpha=alpha)

    self.inference = inference

    self.latent, = inference(X)
    self.latent_shape = get_output_shape(inference.outputs[0])[1:]

    zero = tf.constant(0, dtype='float32')

    ### scaling regularization so that the optimal value of reg_coef
    ### stays more or less independent of network architecture
    correction = sum([
      glorot_scaling(param.shape, gain=1) * np.prod(param.shape)
      for param in inference.parameters(weights=True)
    ])

    self.reg_coef = tf.constant(reg_coef / 2 / correction, dtype='float32')

    broadcast = (None, ) + tuple(slice(None, None, None) for _ in self.latent_shape)

    ### does not appear to work
    self.center = tf.Variable(
      initial_value=tf.ones(self.latent_shape, dtype='float32')
    )
    #self.center = tf.ones(shape=self.latent_shape, dtype='float32')

    distance = tf.reduce_sum(
      (self.latent - self.center[broadcast]) ** 2,
      axis=list(range(1, len(self.latent_shape) + 1))
    )

    if alpha is not None:
      self.R_raw = tf.Variable(
        initial_value=tf.ones(shape=tuple(), dtype='float32')
      )
      self.R = tf.nn.softplus(self.R_raw)

      nu = alpha / (1 + alpha)
      self.rnu = tf.constant(1 / nu, dtype='float32')

      clipped_distance = self.rnu * tf.reduce_mean(
        w * tf.maximum(zero, distance - self.R ** 2)
      ) + self.R ** 2
    else:
      clipped_distance = tf.reduce_mean(distance)

    reg, _ = self.inference.reg_l2()

    self.loss = clipped_distance + self.reg_coef * reg

    variables = inference.variables() + (
      [self.R_raw] if alpha is not None else []
    )

    self.gradients = list(zip(
      variables,
      tf.gradients(
        self.loss, variables,
        stop_gradients=[X]
      )
    ))

    center_reset = tf.assign(self.center, tf.reduce_mean(self.latent, axis=0))
    self._reset = [
      center_reset
    ] + (
      [self.R_raw.initializer] if alpha is not None else []
    )

    super(DeepSVDD, self).__init__(alpha=alpha, reg_coef=reg_coef)

  def __call__(self, X):
    import tensorflow as tf

    code, = self.inference(X)

    broadcast = (None,) + tuple(slice(None, None, None) for _ in self.latent_shape)

    distance = tf.reduce_sum(
      (code - self.center[broadcast]) ** 2,
      axis=list(range(1, len(self.latent_shape) + 1))
    )
    return -tf.log(distance + 1e-6)

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    return self._reset

  def updates(self):
    import tensorflow as tf

    return tf.no_op()

deep_svdd = lambda reg_coef=1: lambda svdd: lambda X_pos, X_neg, alpha, bounding_box: \
  DeepSVDD(svdd, X_pos, X_neg, reg_coef=reg_coef, alpha=alpha)

deep_svdd_oc = lambda reg_coef=1: lambda svdd: lambda X_pos, X_neg, alpha, bounding_box: \
  DeepSVDD(svdd, X_pos, X_neg, reg_coef=reg_coef, alpha=None)